/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

 import React from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   TextInput,
   Button,
   useColorScheme,
   View,
   Alert,
 } from 'react-native';

 import {
   Colors,
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';

 const ViewStyleProps = () => {
  return (
    
      <View style={styles.container}>
      <Text style={styles.html}>YOLO SYSTEM</Text>
      <TextInput
        style={styles.input}
        
        placeholder="Tên đăng nhập"
        keyboardType="numeric"
      />
       <TextInput
        style={styles.input}
        
        placeholder="Mật khẩu"
        keyboardType="numeric"
      />
       <Text style={styles.html_1}>Or</Text>
      <View style={styles.bt}>
      <Button
        title="Login"
        onPress={() => Alert.alert('Dang nhap')}
      />
      
      </View>
      <View style={styles.bt_1}>
      <Button
        color="red"
        title="Google"
        onPress={() => Alert.alert('GG')}
      />
      
      </View>
      <View style={styles.bt_2}>
      <Button
        color="#7fff00"
        title="Facebook"
        onPress={() => Alert.alert('FB')}
      />
      
      </View>
      </View>
     
  );
}

const styles = StyleSheet.create({
  html:{
    textAlign:'center',
    fontSize: 30,
    color:'red',
 
  },
  html_1:{
    textAlign:'center',
    fontSize: 40,
    color:'red',
 
  },
 bt:{
  height: 40,
  margin: 12,
 
  borderRadius: 40,
  
 },
 bt_1:{
  height: 40,
  margin: 12,
  borderRadius: 40,
  
 },
 bt_2:{
  height: 40,
  margin: 12,
  
  borderRadius: 40,
  
 },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderRadius: 10,
  },
container: {
  top: 200,
  borderWidth: 1,
  paddingTop:1,
  position: 'relative', 
 
  margin: 3,
  borderRadius: 10,
},

});

export default ViewStyleProps;


 